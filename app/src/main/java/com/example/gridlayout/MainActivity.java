package com.example.gridlayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    FrameLayout btnProfile;
    FrameLayout btnCity;
    FrameLayout btnQuiz;
    FrameLayout btnExit;
    FrameLayout btnFamily;
    FrameLayout btnEdu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCity=findViewById(R.id.btnCity);
        btnProfile=findViewById(R.id.btnProfile);
        btnQuiz=findViewById(R.id.btnQuiz);
        btnExit=findViewById(R.id.btnExit);
        btnFamily=findViewById(R.id.btnFamily);
        btnEdu=findViewById(R.id.btnEdu);

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Profile terpilih", Toast.LENGTH_SHORT).show();
                Intent beach= new Intent(MainActivity.this,Profile.class);
                startActivity(beach);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Aplikasi Keluar", Toast.LENGTH_SHORT).show();
                System.exit(0);
                finish();
            }
        });

    }
}
